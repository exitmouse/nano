"use strict";
const fs = require('fs');
const inspect = require('util').inspect;
const peg = require('pegjs');
const readline = require('readline');

const util = require('../util.js');

global.roll1 = sides => {
  return 1 + Math.floor(Math.random() * Math.floor(sides));
};

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true,
  prompt: '> ',
});

(async function() {
  let grammar = await fs.readFileAsync('roll.pegjs', 'utf8');
  let parser = peg.generate(grammar);

  rl.prompt();
  rl.on('line', function(line){
    if (!line.trim()) {
      rl.prompt();
      return;
    }
    try {
      let p0;
      let p = parser.parse(line);
      console.log('->', p.toString());
      const MAX_STEPS = 8;
      let i;
      for (i = 0; i < MAX_STEPS; ++i) {
        const stepped = p.step();
        if (!stepped) {
          break;
        }
        console.log('=>', p.toString());
      }
      if (p.comment) { console.log(' #', p.comment); }
      console.log(`** finished in ${i} steps **`);
      console.log();
    } catch (e) {
      console.error(`${e.constructor.name}: ${e.message}`);
      //console.log(e);
    }
    rl.prompt();
  });
})();
