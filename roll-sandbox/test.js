"use strict";
const fs = require('fs');
const inspect = require('util').inspect;
const peg = require('pegjs');

const util = require('../util.js');

global.roll1 = sides => {
  return 1 + Math.floor(Math.random() * Math.floor(sides));
};

const tests = [
  "",

  "1",
  "10",

  "12.34",
  "12.",
  ".12",

  "1+2",
  "1*2",
  "1*2-3/4",

  "-1",
  "-12.34",
  "1/4",

  "d",
  "d4",
  "2d",
  "10d20",
  "1d",
  "0d",
  "(-1)d",
  "d(-1)",
  "(-1)xd",

  // TODO: add this syntax?
  //"ad20",  // 'max(2xd20)
  //"dd20",  // 'min(2xd20)
  // TODO: and maybe this syntax?
  //"3ad20",  // 'max(3xd20)
  //"3dd20",  // 'min(3xd20)

  "2x4",
  "2xd20",
  "2xd",
  "2x3d",
  "2x3d20",
  "2x3d20+4",
  "2x3d20*4",
  "4+2x3d20",
  "4*2x3d20",

  "(1)",
  "d(d20)",
  "2x(3d20+4)",
  "2x3d(d20)",
  "(d1)xd",
  "(d4)x(d4)d(d20)",
  "(d)x(d)d(d)",
  "(d)x(d)d",

  "(0+2)x(d2)d(d2) * (1+1)x(d2)d(d2) + (2+0)x(d2)d(d2)",
  "2x(d20)d1",

  "d(2x2)",
  "(2x2)d",
  "(2x2)xd",

  "1,1",

  "'zero",
  "'roll1(20)",
  "'map('max, 10x(2xd20))",

  "0 # o u4(#$(' 3",
  "0 #o u4(#$(' 3",
  "0# o u4(#$(' 3",
  "0#o u4(#$(' 3",
];

(async function() {
  let grammar = await fs.readFileAsync('roll.pegjs', 'utf8');
  try {
    let parser = peg.generate(grammar);
    for (let line of tests) {
      console.log('>', line);
      const p = parser.parse(line);
      console.log('->', p.toString());
      //console.log(inspect(p.root, {depth: null}));
      let i = 0;
      for (let step of p.iterateStepPrints()) {
        console.log(`${i++}: ${step}`);
      }
      if (p.comment) { console.log(' #', p.comment); }
      console.log();
    }
  } catch (e) {
    console.error(e);
  }
})();
