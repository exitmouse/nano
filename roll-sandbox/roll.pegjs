{
const root = global || this;
const zero = 0;

root.reduce = (f, xs, x0) => xs.reduce(f, x0);
root.map = (f, xs) => xs.map(f);
root.max = x => Math.max.apply(null, x);
root.min = x => Math.min.apply(null, x);

const topEval = root.topEval || (s => { return eval(s); });

const tryFix = (node, fn) => {
  const MAX_STEPS = 200;
  let stepped = false;
  let st;
  for (let i = 0; i < MAX_STEPS; ++i) {
    [node, st] = fn(node);
    stepped |= st;
    if (!st) {
      return [node, stepped];
    }
    //console.log('...', JSON.stringify(node));
  }
  return [node, stepped];
};

const fromRaw = raw => {
  if (raw instanceof Array) {
    return new Vector({ elems: raw.map(fromRaw) });
  }
  return new ScalarValue(raw);
};

class MathError extends Error {}
class Assertion extends Error {}
const assert = (cond, msg) => { if (!cond) { throw new Assertion(msg); } };
const notreached = msg => { assert(false, msg); };

class Tree {
  constructor(root, comment) {
    this.root = root;
    this.comment = comment ? comment.trim() : null;
  }
  * iterateStepPrints() {
    const MAX_STEPS = 100;
    let i;
    for (i = 0; i < MAX_STEPS; ++i) {
      const stepped = this.step()
      if (!stepped) { break; }
      yield this.toString();
    }
    if (i === 0) {
      yield this.toString();
    }
  }
  step() {
    let stepped;
    [this.root, stepped] = tryFix(this.root, n => n.smallStep());
    if (stepped) { return true; }
    [this.root, stepped] = this.root.invokeStep();
    if (stepped) { return true; }
    return false;
  }
  toString() { return this.root.toString(); }
};

class Node {
  constructor(args) {
    Object.assign(this, args);
  }
  mapInPlace(fn) {
    let stepped = false;
    for (const prop in this) {
      if (this.hasOwnProperty(prop)) {
        const [mapped, st] = fn(this[prop]);
        this[prop] = mapped;
        stepped |= st;
      }
    }
    return stepped;
  }
  // Parallel small step. This is, in a sense, pure.
  smallStep() {
    return [this, this.mapInPlace(m => m.smallStep())];
  }
  // Invokes function calls, and rolls dice.
  invokeStep() {
    return [this, this.mapInPlace(m => m.invokeStep())];
  }
  clone() {
    const clone = new this.constructor();
    for (const prop in this) {
      if (this.hasOwnProperty(prop)) {
        const v = this[prop];
        clone[prop] = (v instanceof Node) ? v.clone() : v;
      }
    }
    return clone;
  }
  isValue() { return false; }
  raw() { notreached("raw() unimplemented for " + this.constructor.name); }
  toString() { notreached("toString() unimplemented for " + this.constructor.name); }
};

class ScalarValue {
  constructor(value) {
    this.value = value;
  }
  smallStep() { return [this, false]; }
  invokeStep() { return [this, false]; }
  isValue() { return true; }
  raw() { return this.value; }
  plus(o) {
    if (o instanceof Vector) { return o.plus(this); }
    return new ScalarValue(this.value + o.value);
  }
  times(o) {
    if (o instanceof Vector) { return o.times(this); }
    return new ScalarValue(this.value * o.value);
  }
  neg() { return new ScalarValue(-this.value); }
  recip() { return new ScalarValue(1 / this.value); }
  clone() { return this; }
  toString() {
    // TODO: find a way to avoid printing functions (print the idents instead)
    if (this.value instanceof Function) { return `'{${this.value}}`; }
    return this.value.toString();
  }
}
const ZERO = new ScalarValue(0);
const ONE = new ScalarValue(1);
class Vector extends Node {
  mapInPlace(fn) {
    let stepped = false;
    for (let i = 0; i < this.elems.length; ++i) {
      if (this.elems[i] instanceof Node) {
        const [mapped, st] = fn(this.elems[i]);
        this.elems[i] = mapped;
        stepped |= st;
      }
    }
    return stepped;
  }
  plus(o) {
    assert(this.isValue(), "vector is not a value yet");
    let accessor;
    if (o instanceof ScalarValue) {
      accessor = i => o;
    } else if (o instanceof Vector) {
      if (o.elems.length != this.elems.length) {
        throw new MathError("vector plus different size vector");
      }
      accessor = i => o.elems[i];
    }
    const r = [];
    for (let i = 0; i < this.elems.length; ++i) {
      r[i] = this.elems[i].plus(accessor(i));
    }
    return new Vector({ elems: r });
  }
  times(o) {
    assert(this.isValue(), "vector is not a value yet");
    let accessor;
    if (o instanceof ScalarValue) {
      accessor = i => o;
    } else if (o instanceof Vector) {
      if (o.elems.length != this.elems.length) {
        throw new MathError("vector times different size vector");
      }
      accessor = i => o.elems[i];
    }
    const r = [];
    for (let i = 0; i < this.elems.length; ++i) {
      r[i] = this.elems[i].times(accessor(i));
    }
    return new Vector({ elems: r });
  }
  neg() { return new Vector({ elems: this.elems.map(e => e.neg()) }); }
  recip() { return new Vector({ elems: this.elems.map(e => e.recip()) }); }
  clone() { return new Vector({ elems: this.elems.map(e => e.clone()) }); }
  isValue() { return this.elems.every(e => e.isValue()); }
  raw() { return this.elems.map(e => e.raw()); }
  toString() {
    if (this.elems.length === 0) { return '()'; }
    if (this.elems.length === 1) { return `(${this.elems[0].toString()},)`; }
    return '(' + this.elems.join(', ') + ')';
  }
};
class Sum extends Node {
  smallStep() {
    const [mapped, st] = super.smallStep();
    if (st) { return [mapped, true]; }

    if (this.terms.isValue()) {
      const sum = this.terms.elems.reduce(
          (sum, term) => sum.plus(term), ZERO);
      return [sum, true];
    }
    return [this, false];
  }
  toString() {
    const es = this.terms.elems;
    if (es.length === 0) { return '0'; }
    if (es.length === 1) { return es[0].toString(); }
    return '(' + es[0].toString() + es.slice(1).map(e => {
      return e instanceof Neg ? ` - ${e.value}` : ` + ${e}`;
    }).join('') + ')';
  }
};
class Neg extends Node {
  smallStep() {
    const [mapped, st] = super.smallStep();
    if (st) { return [mapped, true]; }

    if (this.value instanceof ScalarValue) {
      return [this.value.neg(), true];
    }
    return [this, false];
  }
  toString() { return `-${this.value}`; }
};
class Product extends Node {
  smallStep() {
    const [mapped, st] = super.smallStep();
    if (st) { return [mapped, st]; }

    if (this.factors.isValue()) {
      const prod = this.factors.elems.reduce(
          (prod, factor) => prod.times(factor), ONE);
      return [prod, true];
    }

    return [this, false];
  }
  toString() {
    const es = this.factors.elems;
    if (es.length < 2) { throw Bug("product of 1 or fewer items"); }
    return '(' + es[0].toString() + es.slice(1).map(e => {
      return e instanceof Recip ? ` / ${e.value}` : ` * ${e}`;
    }).join('') + ')';
  }
};
class Recip extends Node {
  smallStep() {
    const [mapped, st] = super.smallStep();
    if (st) { return [mapped, true]; }

    if (this.value instanceof ScalarValue) {
      return [this.value.recip(), true];
    }
    return [this, true];
  }
};
class Repeat extends Node {
  invokeStep() {
    let st;
    [this.times, st] = this.times.invokeStep();
    return [this, st];
  }
  smallStep() {
    const [mapped, st] = super.smallStep();
    if (st) { return [mapped, true]; }

    if (this.times instanceof ScalarValue) {
      const ns = [];
      for (let i = 0; i < this.times.value; ++i) {
        ns[i] = this.what.clone();
      }
      return [new Vector({ elems: ns }), true];
    }
    if (this.times instanceof Vector) {
      const ns = [];
      for (let i = 0; i < this.times.elems.length; ++i) {
        ns[i] = new Repeat({ times: this.times.elems[i], what: this.what.clone() });
      }
      return [new Vector({ elems: ns }), true];
    }
    return [this, false];
  }
  toString() {
    let t = this.times instanceof ScalarValue ? `${this.times}` : `(${this.times})`;
    let w = this.what.toString();
    return `${t}x${w}`;
  }
};
class Dice extends Node {
  invokeStep() {
    let st;
    [this.count, st] = this.count.invokeStep();
    if (st) { return [this, true]; }

    assert(this.count.isValue(),
           "Dice.count should either step, or be a value");
    if (this.count instanceof ScalarValue) {
      const ns = [];
      for (let i = 0; i < this.count.value; ++i) {
        [ns[i], st] = this.die.clone().invokeStep();
      }
      return [new Sum({ terms: new Vector({ elems: ns }) }), true];
    }
    if (this.count instanceof Vector) {
      const ns = [];
      for (let i = 0; i < this.count.elems.length; ++i) {
        [ns[i], st] = new Dice({ count: this.count.elems[i],
                                 die: this.die.clone() }).invokeStep();
      }
      return [new Vector({ elems: ns }), true];
    }
    return [this, false];
  }
  toString() {
    let c = this.count instanceof ScalarValue ? `${this.count}` : `(${this.count})`;
    let d = this.die instanceof Die ? `${this.die}` : `(${this.die})`;
    return c + d;
  }
};
class Die extends Node {
  invokeStep() {
    const [mapped, st] = super.invokeStep();
    if (st) { return [mapped, st]; }

    if (this.sides instanceof ScalarValue) {
      return [new ScalarValue(roll1(this.sides.value)), true];
    }
    if (this.sides instanceof Vector) {
      const elems = [];
      for (let i = 0; i < this.sides.elems.length; ++i) {
        let st;
        [elems[i], st] = new Die({ sides: this.sides.elems[i] }).invokeStep();
      }
      return [new Vector({ elems: elems }), true];
    }
    return [this, false];
  }
  toString() {
    let s = this.sides instanceof ScalarValue ? `${this.sides}` : `(${this.sides})`;
    return `d${s}`;
  }
};
class FnCall extends Node {
  invokeStep() {
    const [mapped, st] = super.invokeStep();
    if (!st) {
      assert(this.fn.isValue(),
             "tried to fncall a non-value");
      assert(this.args.isValue(),
             "tried to fncall with non-value args (should have stepped)");
      const result = this.fn.raw().apply(null, this.args.raw());
      return [fromRaw(result), true];
    }
    return [mapped, st];
  }
  toString() {
    return `${this.fn}${this.args}`;
  }
};

}

Tree
  = root:Expression comment:(_ '#' _ Comment)? _ {
      return new Tree(root, comment ? comment[3] : null);
    }
Comment = .* { return text(); }
Expression = Vector
Vector
  = elems:(Element (_ ',' _ Element)* ','?)? {
      if (!elems) { return new Vector({ elems: [] }); }
      if (elems[1].length === 0 && elems[2] != ',') { return elems[0]; }
      const r = [];
      r.push(elems[0]);
      for (const e of elems[1]) {
        r.push(e[3]);
      }
      return new Vector({ elems: r });
    }
Element = Sum
Sum
  = h:(Sign? _ Term) tail:(_ Sign _ Term)* {
      const makeTerm = (neg, e) => neg ? new Neg({ value: e }) : e;
      h = makeTerm(h[0] === '-', h[2]);
      if (tail.length === 0) { return h; }
      const terms = [h];
      for (const t of tail) {
        terms.push(makeTerm(t[1] === '-', t[3]));
      }
      return new Sum({ terms: new Vector({ elems: terms }) });
    }
Sign = '+' / '-'
Term = Product
Product
  = h:Factor tail:(_ Reciprocality _ Factor)* {
      if (tail.length === 0) { return h; }
      const makeFactor = (recip, e) => recip? new Recip({ value: e }) : e;
      const factors = [h];
      for (const t of tail) {
        factors.push(makeFactor(t[1] === '/', t[3]));
      }
      return new Product({ factors: new Vector({ elems: factors }) });
    }
Reciprocality = '*' / '/'
Factor
  = FnCall
  / Extern
  / Repeat
  / Group
  / Number
Repeat
  = Dice
  / times:(Integer / Group) _ 'x' _ what:(Dice / Number / Group) {
      return new Repeat({ times: times, what: what });
    }
Dice
  = Die
  / count:(Integer / Group) _ die:Die {
      return new Dice({ count: count, die: die });
    }
Die
  = 'd' sides:(Integer / Group)? {
      sides = sides || new ScalarValue(6);
      return new Die({ sides: sides });
    }
Group = '(' _ expr:Expression _ ')' { return expr; }
Number
  = Decimal
  / Integer
Integer
  = [0-9]+ {
      return new ScalarValue(parseInt(text(), 10));
    }
Decimal
  = ([0-9]+ '.' [0-9]* / '.' [0-9]+) {
      return new ScalarValue(parseFloat(text()));
    }

FnArgs
  = elems:(Element (_ ',' _ Element)* ','?)? {
      if (!elems) { return new Vector({ elems: [] }); }
      const r = [];
      r.push(elems[0]);
      for (const e of elems[1]) {
        r.push(e[3]);
      }
      return new Vector({ elems: r });
    }
FnCall
  = fn:Extern _ '(' _ args:FnArgs _ ')' {
      return new FnCall({ fn: fn, args: args });
    }

Extern
  = _ name:Ident _ { return new ScalarValue(topEval(name)); }
Ident
  = "'" [a-zA-Z_][a-zA-Z0-9_]* { return text().slice(1); }

_ 'whitespace'
  = [ \t\n\r]*
