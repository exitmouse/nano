"use strict";
const fs = require('fs');
const Discord = require('discord.js');

const util = require('./util.js');

const verbs = new Map([
  ...require('./roll.js').verbs,
]);

const client = new Discord.Client();

client.on('ready', () => {
  console.log(`Logged in as: ${client.user.tag}`);
});

client.on('message', msg => {
  if (msg.content[0] === '/' || msg.content[0] === '\\') {
    let verb = msg.content.split(" ", 1)[0].slice(1);
    let args = msg.content.slice(2 + verb.length);
    for (let [v, handler] of verbs) {
      if (v === verb) {
        try {
          handler(msg, args);
        } catch(e) {
        }
      }
    }
  }
});

fs.readFileAsync('token.txt', 'utf8')
  .then(data => {
    client.login(data.trim());
  }, console.error);
