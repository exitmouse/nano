"use strict";
const fs = require('fs');

fs.readFileAsync = (filename, encoding) => {
  return new Promise((resolve, reject) => {
    try {
      fs.readFile(filename, encoding, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    } catch (err) {
      reject(err);
    }
  });
};
