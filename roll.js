"use strict";
const fs = require('fs');
const peg = require('pegjs');
const vm2 = require('vm2');

var exports = module.exports = {};

exports.verbs = new Map();

async function getVM(msg) {
  if (!msg.channel.vm) {
    await msg.reply('(creating a VM for this channel...)')

    let initSource = await fs.readFileAsync('roll-sandbox/init.js', 'utf8');
    let grammar = await fs.readFileAsync('roll-sandbox/roll.pegjs', 'utf8');
    let parserSource = peg.generate(grammar, {
      exportVar: 'parser',
      format: 'globals',
      output: 'source',
    });
    //console.log(parserSource);

    msg.channel.vm = new vm2.VM({timeout: 5000});
    msg.channel.vm.run(initSource);
    msg.channel.vm.run(parserSource);
    msg.channel.vm.run('const parse = parser.parse;');
  }
  return msg.channel.vm;
}

async function say(msg, text) {
  text = text.toString();
  // '@' + user#0000 (37) + '(' + alias (32) + '),\n... ' + 1900 < max (2000)
  //  1               37     1           32     7           1900 <      2000
  if (text.length > 1900) {
    text = '\n... ' + text.slice(text.length - 1900, text.length);
  }
  //await msg.reply(text);
  let pre = msg.author.toString();
  const alias = getAlias(msg);
  if (alias) {
    pre += ` (${alias})`;
  }
  pre += ', ';
  await msg.channel.send(pre + text);
}

exports.verbs.set('help', async function(msg) {
  await say(msg, `
/rollas [alias]
/roll [rollexpression]
/exec [javascript]
`);
});

exports.verbs.set('exec', async function(msg, args) {
  try {
    let vm = await getVM(msg);
    let result = msg.channel.vm.run(args);
    await say(msg, `**=>** ${result}`);
  } catch (e) {
    await say(msg, `**error** ${e}`);
  }
});

const aliases = new Map();
function aliasesKeyForMessage(msg) {
  return `${msg.channel}:${msg.author}`;
}
function getAlias(msg) {
  return aliases.get(aliasesKeyForMessage(msg));
}
function setAlias(msg, alias) {
  const key = aliasesKeyForMessage(msg);
  if (alias) {
    aliases.set(key, alias);
  } else {
    aliases.delete(key);
  }
}

exports.verbs.set('rollas', async function(msg, args) {
  setAlias(msg, args.trim().slice(0, 32));
  await say(msg, 'OK');
});

exports.verbs.set('roll', async function(msg, args) {
  // This doesn't actually have to be "secure".
  // It's just done to reduce surprises.
  args = args.replace(/"/g, "'");
  try {
    let vm = await getVM(msg);
    let code = `{
      const p = parse("${args}");
      let steps = '';
      for (let step of p.iterateStepPrints()) {
        steps += '\\n**=>** ' + step;
      }
      if (p.comment) { steps += ' # ' + p.comment; }
      steps
    }`;
    let response = msg.channel.vm.run(code);
    await say(msg, response);
  } catch (e) {
    await say(msg, `**${e.constructor.name}:** ${e.message}`);
  }
});
